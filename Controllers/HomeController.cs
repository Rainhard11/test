﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Test1.Models;
using System.Net.Http;
using static System.Net.WebRequestMethods;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Test1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly AppDbContext _appDbContext;

        public HomeController(ILogger<HomeController> logger, AppDbContext appDbContext)
        {
            _logger = logger;
            _appDbContext = appDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SaveData(InputDTO input)
        {
            
            string resultJsonBarang = await RequestApi(input.KodeBarang, "https://insw-dev.ilcs.co.id/my/n/barang?hs_code=");
            if (string.IsNullOrEmpty(resultJsonBarang))
            {
                return NotFound();
            }
            ResponApiDTO dataBarang = JsonConvert.DeserializeObject<ResponApiDTO>(resultJsonBarang);

            string resultJsonTarif = await RequestApi(input.KodeBarang, "https://insw-dev.ilcs.co.id/my/n/tarif?hs_code=");
            if (string.IsNullOrEmpty(resultJsonTarif))
            {
                return NotFound();
            }
            ResponApiDTO dataTarif = JsonConvert.DeserializeObject<ResponApiDTO>(resultJsonTarif);

            var nilaiBm = input.NilaiKomoditas * dataTarif.data[0].bm / 100;
            Guid myuuid = Guid.NewGuid();

            BiayaImpor saveRespon = new BiayaImpor
            {
                IdSimulasi = myuuid.ToString(),
                KodeBarang = dataBarang.data[0].hs_code_format.ToString(),
                UraianBarang = dataBarang.data[0].uraian_id,
                Bm = dataTarif.data[0].bm,
                NilaiKomoditas = input.NilaiKomoditas,
                NilaiBm = nilaiBm,
                WaktuInsert = DateTime.Now
            };

            _appDbContext.BiayaImpors.Add(saveRespon);
            await _appDbContext.SaveChangesAsync();

            return View("Index");
        }

        private async Task<string> RequestApi(string kodeBarang, string url)
        {
            string jsonResult = string.Empty;
            using (HttpClient httpClient = new HttpClient())
            {
                var param = kodeBarang;
                var apiUrl = $"{url}{param}";
                HttpResponseMessage respon = await httpClient.GetAsync(apiUrl);
                if (respon.IsSuccessStatusCode)
                {
                    jsonResult = await respon.Content.ReadAsStringAsync();
                }


            }
            return jsonResult;

        }

    }

    public class InputDTO
    {
        public string KodeBarang { get; set; }
        public int NilaiKomoditas { get; set; }

    }
    public class ResponDTO
    {
        public string id { get; set; }
        public string Kode_barang { get; set; }
        public string Uraian_barang { get; set; }
        public int? Bm { get; set; }
        public float? Nilai_komoditas { get; set; }
        public float? Nilai_bm { get; set; }
        public DateTime? Waktu_insert { get; set; }

    }

    public class ResponApiDTO
    {
        public List<ResponData> data { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }

    public class ResponData
    {
        public int? hs_code_format { get; set; }
        public string uraian_id { get; set; }
        public string sub_header { get; set; }
        public int? hs_code { get; set; }
        public int? bm { get; set; }
        public int? ppnbm { get; set; }
        public int? cukai { get; set; }
        public string bk { get; set; }
        public string ppnbk { get; set; }

    }
}