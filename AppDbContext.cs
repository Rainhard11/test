﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Test1;

public partial class AppDbContext : DbContext
{
    public AppDbContext()
    {
    }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BiayaImpor> BiayaImpors { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TEST;User Id=Test;Password=Test;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BiayaImpor>(entity =>
        {
            entity.ToTable("biaya_impor");

            entity.Property(e => e.IdSimulasi)
                .IsUnicode(false)
                .HasColumnName("Id_simulasi");
            entity.Property(e => e.KodeBarang)
                .HasMaxLength(8)
                .IsUnicode(false)
                .HasColumnName("Kode_barang");
            entity.Property(e => e.NilaiBm).HasColumnName("Nilai_bm");
            entity.Property(e => e.NilaiKomoditas).HasColumnName("Nilai_komoditas");
            entity.Property(e => e.UraianBarang)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasColumnName("Uraian_barang");
            entity.Property(e => e.WaktuInsert)
                .HasColumnType("datetime")
                .HasColumnName("Waktu_insert");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
