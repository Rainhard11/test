CREATE DATABASE TEST
USE [TEST]
GO
/****** Object:  Table [dbo].[biaya_impor]    Script Date: 11/14/2023 6:41:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[biaya_impor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_simulasi] [varchar](max) NULL,
	[Kode_barang] [varchar](8) NULL,
	[Uraian_barang] [varchar](200) NULL,
	[Bm] [int] NULL,
	[Nilai_komoditas] [float] NULL,
	[Nilai_bm] [float] NULL,
	[Waktu_insert] [datetime] NULL,
 CONSTRAINT [PK_biaya_impor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
