﻿using System;
using System.Collections.Generic;

namespace Test1;

public partial class BiayaImpor
{
    public int Id { get; set; }

    public string? IdSimulasi { get; set; }

    public string? KodeBarang { get; set; }

    public string? UraianBarang { get; set; }

    public int? Bm { get; set; }

    public double? NilaiKomoditas { get; set; }

    public double? NilaiBm { get; set; }

    public DateTime? WaktuInsert { get; set; }
}
